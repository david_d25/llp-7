all: main.o mem.o
	gcc -o lab7 main.o mem.o

main.o: main.c mem.o tests.o
	gcc -c -o main.o main.c

tests.o: tests.c mem.o
	gcc -c -o tests.o tests.c

mem.o: mem.h mem.c
	gcc -c -o mem.o mem.c