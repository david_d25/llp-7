#include <unistd.h>

#include "mem.h"

#define HEAP_START ((void*)0x04040000) // TODO do we need it?
#define MMAP_PROT PROT_READ | PROT_WRITE
#define MMAP_FLAGS MAP_PRIVATE | MAP_ANONYMOUS

#pragma pack(push, 1)
struct mem_node {
    // Header
    struct mem_node* next;
    size_t body_capacity;
    bool is_free;

    // Body
    // *data after struct*
};
#pragma pack(pop)

struct mem_node* heap_start;

void* heap_init(size_t initial_size) {
    _malloc(initial_size);
}

void* _malloc(size_t query) {
    if (query == 0)
        return NULL;

    if (query < get_min_malloc_size())
        query = get_min_malloc_size();

    struct mem_node* node = find_suitable_node(query);
    if (!node) node = generate_node(query);
    return take_node(node, query);
}

void _free(void* mem) {
    for (
        struct mem_node *current = heap_start, *previous = NULL;
        current;
        previous = current, current = current->next
    ) {
        if (mem == current+1) {
            current->is_free = true;
            consider_merging(previous, current);
            consider_merging(current, current->next);
            break;
        }
    }
}

struct mem_node* find_suitable_node(size_t query) {
    struct mem_node* current = heap_start;
    while (current != NULL && (!current->is_free || current->body_capacity < query))
        current = current->next;
    return current;
}

struct mem_node* generate_node(size_t query) {
    struct mem_node* last_node = get_last_node(); // Might be NULL

    // printf("DEBUG LAST NODE: %p\n", last_node);
    void* desired_address = get_node_end(last_node);
    size_t length = sizeof(struct mem_node) + query;

    if (length < query) // integer overflow, query is too big
        return NULL;

    if (desired_address == NULL)
        desired_address = HEAP_START;
    // printf("DEBUG DESIRED:   %p (%zu bytes)\n", desired_address, length);

    void* effective_address = mmap(desired_address, length, MMAP_PROT, MMAP_FLAGS, -1, 0);
    // printf("DEBUG EFFECTIVE: %p\n", effective_ыaddress);

    if (effective_address == (void*) -1) // Shit happened?
        return NULL;
    
    struct mem_node* new_node = effective_address;
    new_node->body_capacity = query;
    new_node->is_free = true;
    new_node->next = NULL;

    if (last_node != NULL) {
        last_node->next = new_node;
        bool merged = consider_merging(last_node, new_node);
        if (merged)
            return last_node;
        return new_node;
    } else {
        heap_start = new_node;
        return new_node;
    }
}

struct mem_node* get_last_node() {
    struct mem_node* current = heap_start;
    if (current == NULL)
        return NULL;
    
    while (current->next)
        current = current->next;
    return current;
}

void* take_node(struct mem_node* node, size_t space) {
    if (node == NULL)
        return NULL;

    size_t prev_node_capacity = space;
    ssize_t next_node_capacity = node->body_capacity - space - sizeof(struct mem_node);
    node->is_free = false;

    if (
        node->body_capacity != space && // if queried space and node capacity are not equal...
        next_node_capacity >= get_min_malloc_size() // ...and if the potential new node wouldn't be too small
    ) { // Split the node and take the first one
        node->body_capacity = prev_node_capacity;
        struct mem_node* new_node = get_node_end(node);

        new_node->body_capacity = next_node_capacity;
        new_node->is_free = true;
        new_node->next = node->next;
        
        node->next = new_node;
    }

    return node+1;
}

bool consider_merging(struct mem_node* node_a, struct mem_node* node_b) {
    if (node_a == NULL || node_b == NULL)
        return false;
    
    if (!node_a->is_free || !node_b->is_free)
        return false;
    
    if (node_b->next == node_a) {
        struct mem_node* buffer = node_a;
        node_a = node_b;
        node_b = buffer;
    }

    if (node_a->next != node_b)
        return false;
    
    if (get_node_end(node_a) == node_b) {
        node_a->next = node_b->next;
        node_a->body_capacity += sizeof(struct mem_node) + node_b->body_capacity;
        return true;
    }

    return false;
}

void* get_node_end(struct mem_node* node) {
    if (node == NULL)
        return NULL;

    int8_t* body_start = (int8_t*)(node+1);
    return body_start + node->body_capacity;
}

/**
 * Format:
 * Heap trace:
 * ${index}. [${address}]: ${capacity} bytes, ${free?}
 * ${gap_message_if_exists}
**/
void malloc_debug_full_trace() {
    puts("                    HEAP TRACE");

    struct mem_node* current = heap_start;

    if (current == NULL) {
        puts("──────────────────────────────────────────────────");
        puts("\033[38;5;8m                     (empty)\033[0m");
        puts("──────────────────────────────────────────────────");
        return;
    } else {
        puts("────┬──────────────────┬───────────────────┬──────");
    }

    int i = 0;
    while (current) {
        printf(
            "%3d │ %16p │ %11zu bytes │ %4s\n",
            i + 1,
            current+1,
            current->body_capacity,
            current->is_free ? "\033[38;5;40mFREE\033[0m" : "\033[38;5;196mBUSY\033[0m"
        );
        
        if (current->next)
            puts("────┼─ ── ── ── ── ── ─┼── ── ── ─ ── ── ──┼──────");
        else
            puts("────┴──────────────────┴───────────────────┴──────");

        if (current->next && current->next != get_node_end(current)) {
            puts("\033[38;5;8m                 ~~ memory gap ~~\033[0m");
            puts("────┼─ ── ── ── ── ── ─┼── ── ── ─ ── ── ──┼──────");
        }

        i++;
        current = current->next;
    }
}

void malloc_debug_full_reset() {
    struct mem_node* current = heap_start;
    while (current != NULL) {
        struct mem_node* to_free = current;
        current = current->next;
        munmap(to_free, sizeof(struct mem_node) + to_free->body_capacity);
    }
    heap_start = NULL;
}

size_t get_min_malloc_size() {
    return get_pagesize();
}

size_t get_pagesize() {
    #if defined(LAB7_PAGESIZE) && LAB7_PAGESIZE > 0
        return LAB7_PAGESIZE;
    #else
        return sysconf(_SC_PAGESIZE) - sizeof(struct mem_node);
    #endif
}