#include "mem.h"

#include "tests.c"

void line() {
    puts("\n======================================================================\n");
}

int main() {
    puts("Hello there!");

    test1();
    line();
    test2();
    line();
    test3();
    line();
    test4();
    line();
    test5();
    line();
    test6();
    line();
    test7();
    line();
    test8();
    line();
    test9();

    return 0;
}