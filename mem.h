#ifndef _MEM_H_
#define _MEM_H_

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>

#include <sys/mman.h>
#include <sys/types.h>

#define LAB7_PAGESIZE 9999

struct mem_node;

void* _malloc(size_t query);
void _free(void* mem);
void* heap_init(size_t initial_size);

void malloc_debug_full_trace();
void malloc_debug_full_reset();

static bool consider_merging(struct mem_node* node_a, struct mem_node* node_b);
static void* take_node(struct mem_node*, size_t);
static void* get_node_end(struct mem_node*);
static size_t get_pagesize();
static size_t get_min_malloc_size();
static struct mem_node* get_last_node();
static struct mem_node* generate_node(size_t);
static struct mem_node* find_suitable_node(size_t);

#endif